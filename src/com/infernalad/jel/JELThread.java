package com.infernalad.jel;

import java.util.concurrent.BlockingQueue;

/**
 * Created with IntelliJ IDEA.
 * User: InfernalAd
 * Date: 8/18/13
 * Time: 6:08 PM
 * To change this template use File | Settings | File Templates.
 */
class JELThread {
    private final Thread _thread;
    private JELEventLoop eventLoop;
    private int id;
    private boolean isNamed;


    public JELThread(int id) {
        this.id = id;

        eventLoop = new JELEventLoop(id);
        _thread = new Thread(eventLoop);
        _thread.start();
    }

    public JELThread(int id,BlockingQueue<JELEvent> outerQueue) {
        this.id = id;

        eventLoop = new JELEventLoop(id,outerQueue);
        _thread = new Thread(eventLoop);
        _thread.start();
    }

    public JELEventLoop getEventLoop() {
        return eventLoop;
    }

    public  boolean isWaiting() {
        return eventLoop.isQueueEmpty();
    }

    public int getId() {
        return id;
    }

}
