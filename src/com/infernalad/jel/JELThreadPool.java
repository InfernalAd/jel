package com.infernalad.jel;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: InfernalAd
 * Date: 8/19/13
 * Time: 8:38 PM
 * To change this template use File | Settings | File Templates.
 */
class JELThreadPool {
    private static JELThreadPool instance = new JELThreadPool();
    private final Random rand;
    private Map<Integer, JELThread> basicThreadPool = new HashMap<Integer, JELThread>();
    private Map<String, JELThread> namedThreads = new HashMap<String, JELThread>();

    private BlockingQueue<JELEvent> eventQueue = new LinkedBlockingQueue<JELEvent>();


    private JELThreadPool() {
        int processorNumber = Runtime.getRuntime().availableProcessors();
        Logger.getLogger("main").info("Processors number: " + String.valueOf(processorNumber));
        rand = new Random(1337);
        for (int i = 0; i != processorNumber; i++) {
            int threadId = rand.nextInt(1000);
            while (basicThreadPool.containsKey(threadId)) {
                threadId = rand.nextInt(1000);
            }
            basicThreadPool.put(threadId, new JELThread(threadId, eventQueue));
        }
    }

    public static JELThreadPool getInstance() {
        return instance;
    }

    synchronized void notifyAboutThreadWaiting(Integer id) {
        if (!eventQueue.isEmpty()) {
            JELThread freeThread = basicThreadPool.get(id);
            if (freeThread != null) {
                freeThread.getEventLoop().addEvent(eventQueue.poll());
            }
        }
    }

    public synchronized void runInBackground(Runnable runnable) {
        JELEvent evt = new JELEvent();
        try {
            Method m = Runnable.class.getMethod("run");
            JELSlot newSlot = new JELSlot(runnable, m, JELObject.mainEventLoop);
            evt.setSlot(newSlot);
            eventQueue.add(evt);

        } catch (NoSuchMethodException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void runOnUIThread(Runnable runnable) {
        JELEvent evt = new JELEvent();
        try {
            Method m = Runnable.class.getMethod("run");
            JELSlot newSlot = new JELSlot(runnable, m, JELObject.mainEventLoop);

            evt.setSlot(newSlot);
            JELObject.mainEventLoop.addEvent(evt);

        } catch (NoSuchMethodException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void runOnNamedThread(String threadName,Runnable runnable) {
        JELEvent evt = new JELEvent();
        try {
            Method m = Runnable.class.getMethod("run");
            JELEventLoop eventLoop = getNamedThread(threadName).getEventLoop();
            JELSlot newSlot = new JELSlot(runnable, m, eventLoop);

            evt.setSlot(newSlot);
            eventLoop.addEvent(evt);

        } catch (NoSuchMethodException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }




    public JELThread getNamedThread(String name) {
        if (namedThreads.containsKey(name)) {
            return namedThreads.get(name);
        } else {
            addNamedThread(name);
            return namedThreads.get(name);
        }
    }

    private void addNamedThread(String name) {
        int threadId = rand.nextInt(1000) + 2000;//2000 to 3000
        namedThreads.put(name, new JELThread(threadId));
    }
}
