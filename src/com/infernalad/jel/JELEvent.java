package com.infernalad.jel;


/**
 * Created with IntelliJ IDEA.
 * User: InfernalAd
 * Date: 7/29/13
 * Time: 8:38 PM
 * To change this template use File | Settings | File Templates.
 */
class JELEvent {
    Object sender;
    Class senderClass;
    String signalName;
    JELSlot slot;
    Object[] args;


    public JELEvent setSender(Object sender) {
        this.sender = sender;
        return this;
    }

    public JELEvent setSenderClass(Class senderClass) {
        this.senderClass = senderClass;
        return this;
    }

    public JELEvent setSignalName(String signalName) {
        this.signalName = signalName;
        return this;
    }


    public Object getSender() {
        return sender;
    }

    public Class getSenderClass() {
        return senderClass;
    }

    public String getSignalName() {
        return signalName;
    }

    JELSlot getSlot() {
        return slot;
    }

    JELEvent setSlot(JELSlot slot) {
        this.slot = slot;
        return this;
    }

    Object[] getArgs() {
        return args;
    }

    void setArgs(Object[] args) {
        this.args = args;
    }

}
