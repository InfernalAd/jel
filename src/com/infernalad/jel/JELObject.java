package com.infernalad.jel;

import com.google.common.collect.ArrayListMultimap;
import com.infernalad.jel.com.infernalad.jel.annotations.Signal;
import com.infernalad.jel.com.infernalad.jel.annotations.Signals;
import com.infernalad.jel.com.infernalad.jel.annotations.Slot;

import java.lang.reflect.Method;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: InfernalAd
 * Date: 8/18/13
 * Time: 6:09 PM
 * To change this template use File | Settings | File Templates.
 */
public class JELObject {
    private Map<String,Class<?>[]> signalArgTypes = new HashMap<String, Class<?>[]>();
    private ArrayListMultimap<String, JELSlot> connections = ArrayListMultimap.create();
    private Map<String, JELSlot> slots = new HashMap<String, JELSlot>();
    public static JELEventLoop mainEventLoop;
    private JELEventLoop eventLoop = mainEventLoop;


    public JELObject() {
        Method[] methods = getClass().getDeclaredMethods();
        Slot slotAnnotation = null;
        for (Method method : methods) {
            method.setAccessible(true);
            slotAnnotation = method.getAnnotation(Slot.class);
            if (slotAnnotation != null) {
                registerSlot(slotAnnotation.name(), method);
            }
        }

        Signals signalsList = this.getClass().getAnnotation(Signals.class);
        if (signalsList != null) {
            Signal[] signalNames = signalsList.value();
            for (Signal signal : signalNames) {
                signalArgTypes.put(signal.name(),signal.value());
            }
        }
    }

    private void registerSlot(String name, Method method) {
        if (slots.containsKey(name)) {
            Logger.getLogger("main").log(Level.WARNING, "Slot \"" + name + "\"" + " already exists in class " + getClass().toString()
                    + "; Method " + method.getName() + " was not registered as slot.");
            return;
        } else {
            JELSlot slot = new JELSlot(this, method, this.eventLoop);
            slots.put(name, slot);
        }
    }

    public JELEventLoop getEventLoop() {
        return eventLoop;
    }

    protected void connectSignalToSlot(String signalName, JELSlot slot) {
        if (signalArgTypes.containsKey(signalName)) {
            Class<?>[] signalArgTypes = this.signalArgTypes.get(signalName);
            Class<?>[] slotArgTypes = slot.getArgTypes();
            if (signalArgTypes.length != slotArgTypes.length){
                Logger.getLogger("main").log(Level.SEVERE, "Argument numbers for signal and slot are different.Cannot connect signal to slot.");
                return;
            }
            else {
                for (int i=0;i!=signalArgTypes.length;i++){
                    if (signalArgTypes[i] != slotArgTypes[i]){
                        Logger.getLogger("main").log(Level.SEVERE, "Argument types for signal and slot are different.Cannot connect signal to slot.");
                        return;
                    }
                }
            }


            connections.put(signalName, slot);
        } else {
            Logger.getLogger("main").log(Level.WARNING, "No such signal \"" + signalName + "\" in class " + this.getClass(), toString());
        }
    }

    public void emitSignal(String name, Object... args) {
        if (signalArgTypes.containsKey(name)) {
            if (isArgTypesOk(name,args)) {
                List<JELSlot> slotList = connections.get(name);
                if (slotList != null) {
                    for (JELSlot slot : slotList) {
                        JELEvent evt = new JELEvent().setSlot(slot).setSender(this).setSignalName(name);
                        evt.setArgs(args);
                        slot.getEventLoop().addEvent(evt);
                    }
                }
            }

        } else {
            Logger.getLogger("main").info("No such signal:\"" + name + "\"");
        }
    }

    private boolean isArgTypesOk(String name, Object[] args) {
        Class<?>[] argTypes = signalArgTypes.get(name);
        if (argTypes != null){
            if (argTypes.length != args.length){
                Logger.getLogger("main").log(Level.SEVERE, "Argument numbers for signal and slot are different.Signal emitting aborted.");
                return false;
            }
            else {
                for (int i=0;i!=argTypes.length;i++){
                    if (argTypes[i] != args[i].getClass()){
                        Logger.getLogger("main").log(Level.SEVERE, "Argument types for signal and slot are different.Signal emitting aborted.");
                        return false;
                    }
                }
                return true;
            }
        } else{
            return false;
        }
    }

    public JELSlot getSlot(String name) {
        return this.slots.get(name);
    }


    public static void connect(JELObject sender, String signalName, JELObject reciever, String slotName) {
        JELSlot slot = reciever.getSlot(slotName);
        if (slot == null){
            Logger.getLogger("main").log(Level.SEVERE, String.format("No such slot \"%s\" in class \"%s\".",slotName,reciever.getClass().getName()));
            return;
        }
        sender.connectSignalToSlot(signalName, slot);
    }

    public void moveToThread(JELThread thread) {
        this.eventLoop = thread.getEventLoop();
        for (JELSlot slot : slots.values()) {
            slot.setEventLoop(thread.getEventLoop());
        }
    }


}
