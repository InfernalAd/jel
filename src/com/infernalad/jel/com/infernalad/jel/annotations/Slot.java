package com.infernalad.jel.com.infernalad.jel.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @ com.infernalad.jel.com.infernalad.jel.annotations.Slot
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Slot {
    String name();
}
