package com.infernalad.jel.com.infernalad.jel.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @ com.infernalad.jel.com.infernalad.jel.annotations.Signals
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Signals {
    public Signal[] value();
}
