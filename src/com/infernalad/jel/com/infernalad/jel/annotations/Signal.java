package com.infernalad.jel.com.infernalad.jel.annotations;

/**
 * Created with IntelliJ IDEA.
 * User: shellash
 * Date: 9/3/13
 * Time: 12:38 PM
 * To change this template use File | Settings | File Templates.
 */
public @interface Signal {
    String name();
    Class<?>[] value();
}
