/**
 *@ com.infernalad.jel.JELTimer
 */
package com.infernalad.jel;

import com.infernalad.jel.com.infernalad.jel.annotations.Signal;
import com.infernalad.jel.com.infernalad.jel.annotations.Signals;

import java.util.Timer;
import java.util.TimerTask;

/**
 * JELTimer
 * Class description going here.
 *
 * @author: Mikhail.Shalagin
 * @version: 27.08.13
 */
@Signals({
    @Signal(name = "timeout",value = {})
})
public class JELTimer extends JELObject {
    Timer timer = new Timer();

    public void start(int timeout) {
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                onTimer();
            }
        }, 0, timeout);
    }

    private void onTimer() {
        this.emitSignal("timeout");
    }
}
