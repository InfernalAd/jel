package com.infernalad.jel;

/**
 * Created with IntelliJ IDEA.
 * User: InfernalAd
 * Date: 7/29/13
 * Time: 7:14 PM
 * To change this template use File | Settings | File Templates.
 */
public class JELApplication {
    JELEventLoop mainLoop = new JELEventLoop(1337);


    public JELApplication() {
        JELThreadPool.getInstance();
        JELObject.mainEventLoop = mainLoop;
    }

    public void run() {
        mainLoop.run();
    }

    public static void runOnUiThread(Runnable runnable) {
        JELThreadPool.getInstance().runOnUIThread(runnable);
    }

    public static void runInBackground(Runnable runnable) {
        JELThreadPool.getInstance().runInBackground(runnable);
    }

    public static void runInNamedThread(String threadName,Runnable runnable) {
        JELThreadPool.getInstance().runOnNamedThread(threadName,runnable);
    }

    public static void terminate(int code) {
        System.exit(code);
    }

    public static JELThread getNamedThread(String threadName){
        return JELThreadPool.getInstance().getNamedThread(threadName);
    }

}
