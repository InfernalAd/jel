package com.infernalad.jel;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: InfernalAd
 * Date: 8/25/13
 * Time: 12:08 PM
 * To change this template use File | Settings | File Templates.
 */
class JELSlot {
    private JELEventLoop eventLoop;
    private Object reciever;
    private Method method;

    public JELSlot(Object reciever, Method method, JELEventLoop eventLoop) {
        this.reciever = reciever;
        this.method = method;
        this.eventLoop = eventLoop;
    }

    public void execute(Object[] args) throws InvocationTargetException, IllegalAccessException {
        Class<?>[] parameterTypes = method.getParameterTypes();
        if (parameterTypes.length == 0 && args == null){
            method.invoke(reciever, args);
            return;
        }
        if (parameterTypes.length != args.length) {
            Logger.getLogger("main").log(Level.SEVERE, "Argument numbers for signal and slot are different.Slot execution aborted.");
            return;
        }
        for (int i = 0; i != args.length; i++) {
            if (!args[i].getClass().equals(parameterTypes[i])) {
                Logger.getLogger("main").log(Level.SEVERE, "Argument types for signal and slot are different.Slot execution aborted.");
                return;
            }
        }

        method.invoke(reciever, args);
    }

    public JELEventLoop getEventLoop() {
        return eventLoop;
    }

    public void setEventLoop(JELEventLoop eventLoop) {
        this.eventLoop = eventLoop;
    }

    public Class<?>[] getArgTypes(){
        return method.getParameterTypes();
    }
}
