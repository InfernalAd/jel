package com.infernalad.jel;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: InfernalAd
 * Date: 7/29/13
 * Time: 7:17 PM
 * To change this template use File | Settings | File Templates.
 */
class JELEventLoop implements Runnable {

    private final int threadId;
    BlockingQueue<JELEvent> eventQueue = new LinkedBlockingQueue<JELEvent>();
    private boolean isRunning = true;


    public JELEventLoop(int threadId) {
        this.threadId = threadId;
    }

    public JELEventLoop(int threadId,BlockingQueue<JELEvent> outerQueue) {
        this.threadId = threadId;
        this.eventQueue = outerQueue;
    }

    public void run() {
        while (isRunning) {
            JELEvent event = null;
            try {
                event = eventQueue.take();
                try {
                    event.getSlot().execute(event.getArgs());
                } catch (Exception e) {
                    Logger.getLogger("error").log(Level.WARNING, e.getClass().toString() + e.getMessage());
                    e.printStackTrace();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    public void addEvent(JELEvent event) {
        try {
            eventQueue.put(event);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public boolean isQueueEmpty() {
        return eventQueue.isEmpty();
    }
}
